# countrypicker-ios-swift

Country Picker with flags for iOS iPhone Apps, written in Swift.
Currently support 225 countries.

Put some settings such as,

* DisplayOnlyCountriesArr
* ShowExceptCountriesArr
* enableSearchBar
* enableIndexedScrollBar
* CountryPickerGridViewController
* Theming